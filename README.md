# Temperature Converter API

This is a little toy api I made using Ruby and Sinatra[sinatrarb.com]. It's an example of how one could build a RESTful api with sinatra, although this demo only responds to one get request.

To run this app, make sure you have the sinatra gem installed

```bash
    gem install sinatra
    #optionally, install thin
    gem install thin
```

To test this api run `ruby degreeConverter.rb` and then in another window, you can test the api with a curl command as shown below

```bash
    #the format for the curl command is
    #http://localhost:8080/convertFrom/<unit you want to convert from>/<value of unit you want to convert from>/degrees/to/<unit you want to convert to>
    #here is an example
    curl http://localhost:8080/convertFrom/fahrenheit/-40/degrees/to/celsius
    #That should output -40 on your computer
    #fun fact -40 fahrenheit is equal to -40 celsius
```

You can also test this in your browser if you'd like.

Disclaimer
The formulas I used aren't super accurate. They are pretty close, but can be off(in my testing it was never off more than 0.5 degrees). This little project was a demo I made in like an hour, don't use my formulas for anything mission critical.
