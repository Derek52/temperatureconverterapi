require 'sinatra'
require 'json'

def convertKelvinToFahrenheit(kelvin)
  return 9.0 / 5.0 * (kelvin - 273) + 32
end

def convertKelvinToCelsius(kelvin)
  return kelvin - 273
end

def convertCelsiusToFahrenheit(celsius)
  return ((9.0/5.0) * celsius) + 32
end

def convertCelsiusToKelvin(celsius)
  return celsius + 273
end

def convertFahrenheitToKelvin(fahrenheit)
  return (5.0 / 9.0 * (fahrenheit - 32)) + 273
  #these would also work here(and in a couple other formulas.
  #I find this to be kind of funny, but see no reason to write my methods this way.
  #return convertFahrenheitToCelsius(fahrenheit) + 273
  #return convertCelsiusToKelvin(convertFahrenheitToCelsius(fahrenheit))
end

def convertFahrenheitToCelsius(fahrenheit)
  return 5.0 / 9.0 * (fahrenheit - 32)
end

def badString()
  return "You input an incorrectly formed string"
end

set :port, 8080
set :environment, :production

get '/convertFrom/:startingUnit/:temperature/degrees/to/:convertTo' do
  temp = params[:temperature].to_f
  convertToUnit = params[:convertTo].downcase
  case params[:startingUnit].downcase
    when "kelvin"
      if convertToUnit == "celsius"
        return convertKelvinToCelsius(temp).to_json
      else if convertToUnit == "fahrenheit"
        return convertKelvinToFahrenheit(temp).to_json
      else
        return badString().to_json
      end
    end
    when "celsius"
      if convertToUnit == "fahrenheit"
        return convertCelsiusToFahrenheit(temp).to_json
      else if convertToUnit == "kelvin"
        return convertCelsiusToKelvin(temp).to_json
      else
        return badString().to_json
      end
    end
    when "fahrenheit"
      if convertToUnit == "kelvin"
        return convertFahrenheitToKelvin(temp).to_json
      else if convertToUnit == "celsius"
        return convertFahrenheitToCelsius(temp).to_json
      else
        return badString().to_json
      end
    end
    else
      return badString().to_json
     end
end

#This could be done with a few blocks like this for each starting unit. But I ended up going with the bigger single block for this simple script
#get '/kelvin/:convertTo/:temperature' do
#  temp = params[:temperature].to_f
#  convertToUnit = params[:convertTo].downcase
#  if convertToUnit == "celsius"
#    return convertKelvinToCelsius(temp).to_json
#  else if convertToUnit == "fahrenheit"
#    return convertKelvinToFahrenheit(temp).to_json
#  else
#    return "You input an incorrectly formated URL".to_json
#  end
#end
